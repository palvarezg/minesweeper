import { EventEmitter } from '@angular/core';
import { Component, Output } from '@angular/core';

@Component({
  selector: 'app-game-over',
  templateUrl: './game-over.component.html',
  styleUrls: ['./game-over.component.scss']
})
export class GameOverComponent {
  @Output()
  onResetGame = new EventEmitter<boolean>();

  constructor() { }

  resetGame(): void {
    this.hide();
    this.onResetGame.emit(true);
  }

  show(): void {
    const gameOverScreen = document.getElementById('gameOverScreen');
    gameOverScreen.style.visibility = 'show';
  }

  hide(): void {
    const gameOverScreen = document.getElementById('gameOverScreen');
    gameOverScreen.style.visibility = 'hidden';
  }
}
