import { Component, ViewChild } from '@angular/core';
import { MinegridComponent } from '../minegrid/minegrid.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {
  elapsedTime = 0;
  bombNumber = 0;
  gameOver = false;

  @ViewChild('minegrid', { static: true })
  minegrid: MinegridComponent;

  resetGame(val: boolean) {
    this.minegrid.reset();
    this.gameOver = !val;
  }

  setGameOver(isOver: boolean): void {
    this.gameOver = isOver;
  }

  setBombNumber(bombs: number): void {
    this.bombNumber = bombs;
  }

  setTime(time: number): void {
    this.elapsedTime = time;
  }
}
