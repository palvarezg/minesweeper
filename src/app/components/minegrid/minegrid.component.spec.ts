import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MinegridComponent } from './minegrid.component';

describe('MinegridComponent', () => {
  let component: MinegridComponent;
  let fixture: ComponentFixture<MinegridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MinegridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MinegridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
