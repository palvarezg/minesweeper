import { Component, Output, EventEmitter } from '@angular/core';
import { Minegrid } from 'src/app/models/minegrid';
import { DifficultyEmum } from 'src/app/models/difficulty-enum';
import { CellTypeEnum } from 'src/app/models/cell-type-enum';

@Component({
  selector: 'app-minegrid',
  templateUrl: './minegrid.component.html',
  styleUrls: ['./minegrid.component.scss']
})
export class MinegridComponent {
  grid: number[][];
  bombsGrid: number[][];
  minegrid: Minegrid;

  @Output()
  elapsedTimeEvent: EventEmitter<number>;
  @Output()
  bombNumberEvent: EventEmitter<number>;
  @Output()
  gameOverEvent: EventEmitter<boolean>;

  private elapsedTime: number;
  private timerInterval;
  private dead = false;
  private totalCells: number;
  private revealedCells: number;
  private readonly xCellsNumber = 10;
  private readonly yCellsNumber = 10;

  constructor() {
    this.elapsedTimeEvent = new EventEmitter<number>();
    this.bombNumberEvent = new EventEmitter<number>();
    this.gameOverEvent = new EventEmitter<boolean>();
    this.minegrid = new Minegrid();
    this.reset();
  }

  reset(): void {
    this.revealedCells = 0;
    this.dead = false;
    this.minegrid.generate(this.xCellsNumber, this.yCellsNumber, DifficultyEmum.easy).then(grid => {
      this.totalCells = this.xCellsNumber * this.yCellsNumber;
      this.grid = [...grid];
      this.minegrid.getBombsNumberGrid().then(g => this.bombsGrid = [...g]);
      this.bombNumberEvent.emit(this.minegrid.bombNumber);
      window.clearInterval(this.timerInterval);
      this.startTimer();
    });
  }

  selectCell(y: number, x: number): void {
    if (this.dead) return;

    if (this.grid[y][x] !== CellTypeEnum.bomb) {
      if (!this.showCell(y, x)) return;
      this.showLeftFreeCells(y, x);
      this.showRightFreeCells(y, x);
      this.showDownFreeCells(y, x);
      this.showUpFreeCells(y, x);
      this.showWin();
    } else {
      this.showCell(y, x, true);
      setTimeout(() => {
        this.gameOver();
      }, 100);
      return;
    }
  }

  setFlag(y: number, x: number): void {
    console.log('flag on ' + y + x)
  }

  getNearBombsNumber(y: number, x: number): number {
    return this.bombsGrid[y][x];
  }

  isBomb(y: number, x: number): boolean {
    return this.grid[y][x] === CellTypeEnum.bomb;
  }

  private gameOver(): void {
    this.dead = true;
    this.gameOverEvent.emit(true);
  }

  private startTimer(): void {
    this.elapsedTime = 0;
    this.timerInterval = setInterval(() => {
      this.elapsedTime++;
      this.elapsedTimeEvent.emit(this.elapsedTime);
    }, 1000);
  }

  private showWin(): void {
    if (this.revealedCells + this.minegrid.bombNumber === this.totalCells) {
      alert('GANASTE WEY');
    }
  }

  private showCell(y: number, x: number, isBomb?: boolean): boolean {
    if (this.dead)
      return;

    const element = document.getElementById(`c${y}${x}`);
    if (!element)
      return;

    if (!element.classList.contains('cell-content-visible')) {
      const parent = element.parentElement;
      parent.classList.add('cell-shown');

      if (isBomb)
        parent.classList.add('cell-bomb');

      if (this.bombsGrid[y][x] > 0) {
        element.classList.remove('cell-content');
        element.classList.add('cell-content-visible');
      }

      this.revealedCells++;

      return true;
    } else {
      return false;
    }
  }

  private showLeftFreeCells(y: number, x: number): void {
    let lx = x - 1;
    if (this.minegrid.canMoveLeft(x) && this.grid[y][lx] === CellTypeEnum.bomb) return;

    if (!this.showCell(y, lx)) return;
    this.showLeftFreeCells(y, lx);
    this.showUpFreeCells(y, x);
    this.showDownFreeCells(y, x);
  }

  private showRightFreeCells(y: number, x: number): void {
    let lx = x + 1;
    if (this.minegrid.canMoveRight(x) && this.grid[y][lx] === CellTypeEnum.bomb) return;

    if (!this.showCell(y, lx)) return;
    this.showRightFreeCells(y, lx);
    this.showUpFreeCells(y, x);
    this.showDownFreeCells(y, x);
  }

  private showUpFreeCells(y: number, x: number): void {
    let ly = y - 1;
    if (this.minegrid.canMoveUp(y) && this.grid[ly][x] === CellTypeEnum.bomb) return;

    if (!this.showCell(ly, x)) return;
    this.showUpFreeCells(ly, x);
  }

  private showDownFreeCells(y: number, x: number): void {
    let ly = y + 1;
    if (this.minegrid.canMoveDown(y) && this.grid[ly][x] === CellTypeEnum.bomb) return;

    if (!this.showCell(ly, x)) return;
    this.showDownFreeCells(ly, x);
  }
}
