export enum DifficultyEmum {
    easy = 0.1,
    medium = 0.3,
    hard = 0.5
}