import { DifficultyEmum } from './difficulty-enum';
import { CellTypeEnum } from './cell-type-enum';

export class Minegrid {
    /**
     * grid[y][x]
     */
    grid: number[][];
    bombsGrid: number[][];
    indexesX: number;
    indexesY: number;
    bombNumber: number;

    constructor() {
        this.indexesX = 0;
        this.indexesY = 0;
        this.bombNumber = 0;
    }

    /**
     * 
     * @param xCells number of horizontal cells
     * @param yCells number of vertical cells
     * @param difficulty difficulty of the game. It will affect to the number of bombs
     * @returns a grid with empty cells and bomb cells
     */
    generate(xCells: number, yCells: number, difficulty: DifficultyEmum): Promise<number[][]> {
        this.initialize(xCells, yCells);
        return new Promise<number[][]>((resolve) => {
            for (let y = 0; y < yCells; y++) {
                this.grid[y] = [];
                for (let x = 0; x < xCells; x++) {
                    const rnd = Math.random();
                    if (rnd <= difficulty) {
                        this.grid[y][x] = CellTypeEnum.bomb;
                        this.bombNumber++;
                    } else {
                        this.grid[y][x] = CellTypeEnum.free;
                    }
                }
            }
            resolve(this.grid)
        });
    }

    /**
     * Iterates through the grid and returns the number of
     * near bombs to a cell.
     * @returns a new grid with those numbers
     */
    getBombsNumberGrid(): Promise<number[][]> {
        return new Promise<number[][]>((resolve) => {
            for (let y = 0; y < this.grid.length; y++) {
                this.bombsGrid[y] = [];
                for (let x = 0; x < this.grid[0].length; x++) {
                    this.bombsGrid[y][x] = this.getNearBombsNumber(y, x);
                }
            }
            resolve(this.bombsGrid)
        });
    }

    canMoveRight(x: number): boolean {
        return x < this.indexesX;
    }

    canMoveLeft(x: number): boolean {
        return x > 0;
    }

    canMoveUp(y: number): boolean {
        return y > 0;
    }

    canMoveDown(y: number): boolean {
        return y < this.indexesY;
    }

    private initialize(xCells: number, yCells: number): void {
        this.grid = [];
        this.bombsGrid = [];
        this.bombNumber = 0;
        this.indexesX = xCells - 1;
        this.indexesY = yCells - 1;
    }

    private getNearBombsNumber(y: number, x: number): number {
        let bombNumber = 0;
        //right
        if (this.canMoveRight(x) && this.grid[y][x + 1] === CellTypeEnum.bomb) {
            bombNumber++;
        }
        //left
        if (this.canMoveLeft(x) && this.grid[y][x - 1] === CellTypeEnum.bomb) {
            bombNumber++;
        }
        //up
        if (this.canMoveUp(y) && this.grid[y - 1][x] === CellTypeEnum.bomb) {
            bombNumber++;
        }
        //down
        if (this.canMoveDown(y) && this.grid[y + 1][x] === CellTypeEnum.bomb) {
            bombNumber++;
        }
        //right-up
        if ((this.canMoveRight(x) && this.canMoveUp(y))
            && this.grid[y - 1][x + 1] === CellTypeEnum.bomb) {
            bombNumber++;
        }
        //right-down
        if ((this.canMoveRight(x) && this.canMoveDown(y))
            && this.grid[y + 1][x + 1] === CellTypeEnum.bomb) {
            bombNumber++;
        }
        //left-up
        if ((this.canMoveLeft(x) && this.canMoveUp(y))
            && this.grid[y - 1][x - 1] === CellTypeEnum.bomb) {
            bombNumber++;
        }
        //left-down
        if ((this.canMoveLeft(x) && this.canMoveDown(y))
            && this.grid[y + 1][x - 1] === CellTypeEnum.bomb) {
            bombNumber++;
        }

        return bombNumber;
    }
}
